# Voraussetzungen
Projekt wurde lokal ausgecheckt und notwendige Tools sind installiert.

# Aufgabe 1 - Spring Boot Anwendung bauen
a) Baue die Anwendung ohne die Tests auszuführen

b) Baue die Anwendung mit Ausführung der Tests

# Aufgabe 2 - Spring Boot Anwendung starten
Starte die gebaute Spring Boot Anwendung mit einer beliebigen Möglichkeit deiner Wahl

# Aufgabe 3 - Führe Requests gegen die Anwendung aus
a) Frage über die Schnittstelle alle Personen ab und speicher das Ergebnis in einer Datei

b) Erstelle eine neue Person